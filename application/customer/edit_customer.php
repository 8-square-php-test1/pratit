<?php
include_once $base . "common/dbconfig.php";
$base = '../../includes/';
include $base . 'header.php';

//getting id from url
$id = $_GET['id'];

//selecting data associated with this particular id
// $result = mysqli_query($mysqli, );
$sql = "SELECT * FROM customer WHERE id=$id";
$result = $conn->query($sql);
// mysql_close();
while ($res = mysqli_fetch_array($result)) {

// while ($res = mysqli_fetch_array($result)) {
    $name = $res['customer_name'];
    $address = $res['customer_address'];
}
?>
    <div class="main-content container">
        <div class="row">
            <div class="col-lg-12">

                <div class="page-header">
                    <h2>Edit Record of Customer</h2>
                </div>
                <p>Please make required updates to make change record to the database.</p>

                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">

                    <div class="form-group">
                        <label>Customer Name</label>
                        <input type="text" name="customer_name" class="form-control" value="<?php echo $name; ?>">
                    </div>
                    <div class="form-group">
                        <label>Customer Address</label>
                        <input type="text" name="customer_address" class="form-control" value="<?php echo $address; ?>">
                    </div>

                    <input type="Submit" class="btn btn-primary" value="submit">
                    <a href="../../index.php" class="btn btn-default">Cancel</a>

                </form>
            </div>
        </div>
    </div>

    <?php
include $base . 'footer.php';
?>