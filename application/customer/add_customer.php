<?php
$base = '../../includes/';
include $base . 'header.php';
include_once $base . "common/dbconfig.php";
//including the database connection file

if ($_SERVER["REQUEST_METHOD"] == "POST") {
// if (isset($_POST['Submit'])) {
    $name = $_POST['customer_name'];
    $address = $_POST['customer_address'];
    // $now = time();

    // checking empty fields
    if (empty($name) || empty($address)) {
        if (empty($name)) {
            echo "<font color='red'>Name field is empty.</font><br/>";
        }

        if (empty($address)) {
            echo "<font color='red'>Address field is empty.</font><br/>";
        }
    } else {
        // if all the fields are filled (not empty)

        //insert data to database
        $sql = "INSERT INTO customer (customer_name, customer_address, created_date, modified_date) VALUES('$name','$address',NOW(),NOW())";

        if ($conn->query($sql) === true) {
            // echo "New record created successfully";
            echo "<font color='green'>Data added successfully.</font>";
            echo "<br/><a href='view_customer.php'>View Data</a>";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        //display success message

    }
}
$conn->close();
?>
    <div class="main-content container">
        <div class="row">
            <div class="col-lg-12">

                <div class="page-header">
                    <h2>Create Record of Customer</h2>
                </div>
                <p>Please fill this form and submit to add customer record to the database.</p>

                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">

                    <div class="form-group">
                        <label>Customer Name</label>
                        <input type="text" name="customer_name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Customer Address</label>
                        <input type="text" name="customer_address" class="form-control">
                    </div>

                    <input type="Submit" class="btn btn-primary" value="submit">
                    <a href="../../index.php" class="btn btn-default">Cancel</a>

                </form>
            </div>
        </div>
    </div>
    <?php
include $base . 'footer.php';
?>
