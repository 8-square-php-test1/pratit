<?php
$base = '../../includes/';
include $base . 'header.php';
include_once $base . "common/dbconfig.php";
//including the database connection file

if ($_SERVER["REQUEST_METHOD"] == "POST") {
// if (isset($_POST['Submit'])) {
    $name = $_POST['product_name'];
    $description = $_POST['product_description'];
    // $now = time();

    // checking empty fields
    if (empty($name) || empty($description)) {
        if (empty($name)) {
            echo "<font color='red'>Name field is empty.</font><br/>";
        }

        if (empty($description)) {
            echo "<font color='red'>Description field is empty.</font><br/>";
        }
    } else {
        // if all the fields are filled (not empty)

        //insert data to database
        $sql = "INSERT INTO product (product_name, product_description, created_date, modified_date) VALUES('$name','$description',NOW(),NOW())";

        if ($conn->query($sql) === true) {
            // echo "New record created successfully";
            echo "<font color='green'>Data added successfully.</font>";
            echo "<br/><a href='view_product.php'>View Data</a>";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        //display success message

    }
}
$conn->close();
?>
    <div class="main-content container">
        <div class="row">
            <div class="col-lg-12">

                <div class="page-header">
                    <h2>Create Record of Product</h2>
                </div>
                <p>Please fill this form and submit to add Product record to the database.</p>

                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">

                    <div class="form-group">
                        <label>Product Name</label>
                        <input type="text" name="product_name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Product description</label>
                        <input type="text" name="product_description" class="form-control">
                    </div>

                    <input type="Submit" class="btn btn-primary" value="submit">
                    <a href="../../index.php" class="btn btn-default">Cancel</a>

                </form>
            </div>
        </div>
    </div>
    <?php
include $base . 'footer.php';
?>
