<?php
$base = '../../includes/';
include $base . "header.php";

?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">
            <?php include $base . "common/dbconfig.php";

$sql = "SELECT * FROM product";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    echo "<table class='table'>";
    echo "<tsection>";
    echo "<tr>";
    echo "<th>product ID</th>";
    echo "<th>product Name</th>";
    echo "<th>product description</th>";
    echo "<th>product Created at</th>";
    echo "</tr>";
    echo "</tsection>";
    echo "<tbody>";
    while ($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>" . $row['id'] . "</td>";
        echo "<td>" . $row['product_name'] . "</td>";
        echo "<td>" . $row['product_description'] . "</td>";
        echo "<td>" . $row['created_date'] . "</td>";
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
    mysqli_free_result($result);
} else {
    echo "0 results";
}
$conn->close();
?>
        </div>
    </div>
</div>
<?php include $base . "footer.php";?>