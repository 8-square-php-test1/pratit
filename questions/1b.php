<?php
$base = '../includes/';
include $base . "header.php";

$product_id = $_GET['p_id'] ?? 0;
?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">
            <?php include $base . "common/dbconfig.php";

$sql = "SELECT c.customer_name, o.id
FROM order_products op
JOIN orders o ON o.id=op.order_id
JOIN product p ON op.product_id=p.id AND p.id='1'
JOIN customer c ON c.id=o.customer_id";

//$sql = "SELECT newTable.*, customer.customer_name FROM (SELECT orders.customer_id, order_products.order_id FROM order_products INNER JOIN orders ON orders.id=order_products.order_id AND order_products.product_id='$p_id') AS newTable INNER JOIN customer ON newTable.customer_id=customer.id";

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    echo "<table class='table'>";
    echo "<tsection>";
    echo "<tr>";
    echo "<th>Order ID</th>";
    echo "<th>Customer Name</th>";
    echo "</tr>";
    echo "</tsection>";
    echo "<tbody>";
    while ($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>" . $row['id'] . "</td>";
        echo "<td>" . $row['customer_name'] . "</td>";
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
    mysqli_free_result($result);
} else {
    echo "0 results";
}
$conn->close();
?>
        </div>
    </div>
</div>
<?php include $base . "footer.php";?>