<?php
$base = '../includes/';
include $base . "header.php";
?>
<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">
            <?php include $base . "common/dbconfig.php";

$sql = "SELECT p.product_name, p.id
            FROM order_products op
            JOIN orders o ON o.id=op.order_id AND o.status='1'
            JOIN product p ON op.product_id=p.id";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    echo "<table class='table'>";
    echo "<tsection>";
    echo "<tr>";
    echo "<th>Product ID</th>";
    echo "<th>Product Name</th>";
    // echo "<th>Product Description</th>";
    echo "</tr>";
    echo "</tsection>";
    echo "<tbody>";
    while ($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>" . $row['id'] . "</td>";
        echo "<td>" . $row['product_name'] . "</td>";
        // echo "<td>" . $row['product_description'] . "</td>";
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
    mysqli_free_result($result);
} else {
    echo "0 results";
}
$conn->close();
?>
        </div>
    </div>
</div>
<?php include $base . "footer.php";?>