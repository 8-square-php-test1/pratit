<?php
$base = '../includes/';
include $base . "header.php";
?>

<div class="main-content container">
    <div class="row">
        <div class="col-lg-12">
            <?php include $base . "common/dbconfig.php";
$t1 = strtodate("01/10/2014");
$t2 = time();

$sql = "SELECT c.name, count(op.id) AS 'product_purchased',o.created_date AS 'ordered_date'
FROM order_products op
JOIN orders o ON o.id=op.order_id
JOIN product p ON op.product_id=p.id AND p.id=?
JOIN customer c ON c.id=o.customer_id
GROUP BY o.id
WHERE timestamp>'$t1' AND timestamp<'$t2'";
$result = $conn->query($sql);
