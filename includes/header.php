<?php
$URL = 'http://localhost/assignment/';
$BASE_URL = 'http://localhost/assignment/application/';
?>

<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Store Management System</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo $URL; ?>assets/css/bootstrap/bootstrap.min.css"
    integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>

<body>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo $URL; ?>index.php">Store MS</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Customer</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $BASE_URL; ?>customer/view_customer.php">View Customers</a></li>
                        <li><a href="<?php echo $BASE_URL; ?>customer/add_customer.php">Add Customers</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Product</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $BASE_URL; ?>product/view_product.php">View Product</a></li>
                        <li><a href="<?php echo $BASE_URL; ?>product/add_product.php">Add Product</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</div>
